#include "includes.h"
#include "pkt_window.h"
#include <stdlib.h>
#include <stddef.h>

// ***************************************************************************
// * ALTERNATING BIT AND GO-BACK-N NETWORK EMULATOR: VERSION 1.1  J.F.Kurose
// *
// * These are the functions you need to fill in.
// ***************************************************************************

#define TIMEOUT 25.0


int min(int x, int y)
{
  return x < y ? x : y;
}


// variables for side A
static struct {
  int base;           // sequence number of the oldest unacknowledged packet
  int nextseqnum;     // the smallest unused sequence number (i.e. the next
                      // packet to be sent)
  struct pkt_window *sent_pkts; // stores the unacknowledged packets
} a;


// variables for side B
static struct {
  int expectedseqnum; // the sequence number of the next in-order packet
  struct pkt last_ack;
} b;


// ***************************************************************************
// * The following routine will be called once (only) before any other
// * entity A routines are called. You can use it to do any initialization
// ***************************************************************************
void A_init()
{
  a.base = 1;
  a.nextseqnum = 1;
  pkt_window_init(&a.sent_pkts, 50);
}

// ***************************************************************************
// * The following rouytine will be called once (only) before any other
// * entity B routines are called. You can use it to do any initialization
// ***************************************************************************
void B_init()
{
  b.expectedseqnum = 1;
}


int checksum(struct pkt packet)
{
  unsigned int sum;
  unsigned short *p;

  sum = 0;
  for (p = (unsigned short *) &packet;
       p < ((unsigned short *) &packet) + sizeof(packet)/sizeof(unsigned short);
       ++p) {
    sum += *p;
    /* wrap-around */
    if (sum & 0x00010000) {
      sum -= 0x00010000;
      sum += 1;
    }
  }

  return (int) ~sum;
}


// ***************************************************************************
// * Called from layer 5, passed the data to be sent to other side
// ***************************************************************************
int A_output(struct msg message)
{
  std::cout << "Layer 4 on side A has recieved a message from the application that should be sent to side B: " << message << std::endl;

  struct pkt packet;
  packet.seqnum = a.nextseqnum;
  packet.acknum = 0;
  packet.checksum = 0;
  memcpy(packet.payload, message.data, 20);
  packet.checksum = checksum(packet);

  pkt_window_new_pkt(a.sent_pkts, packet, a.base);

  if (a.base == a.nextseqnum)
    simulation->starttimer(A, TIMEOUT);

  a.nextseqnum++;

  simulation->tolayer3(A, packet);

  return 1;
}


// ***************************************************************************
// * Called from layer 3, when a packet arrives for layer 4 on side A
// ***************************************************************************
void A_input(struct pkt packet)
{
  std::cout << "Layer 4 on side A has recieved a packet sent over the network from side B:" << packet << std::endl;

  int cs = packet.checksum;
  packet.checksum = 0;
  if (cs != checksum(packet))
    return;

  a.base = packet.acknum + 1;
  pkt_window_update_base(a.sent_pkts, a.base);

  simulation->stoptimer(A);
  if (a.base != a.nextseqnum)
    simulation->starttimer(A, TIMEOUT);
}


// ***************************************************************************
// * Called from layer 5, passed the data to be sent to other side
// ***************************************************************************
int B_output(struct msg message)
{
  std::cout << "Layer 4 on side B has recieved a message from the application that should be sent to side A: " << message << std::endl;
  return 1; /* Return a 0 to refuse the message */
}


// ***************************************************************************
// // called from layer 3, when a packet arrives for layer 4 on side B
// ***************************************************************************
void B_input(struct pkt packet)
{
  std::cout << "Layer 4 on side B has recieved a packet from layer 3 sent over the network from side A:" << packet << std::endl;

  int cs = packet.checksum;
  packet.checksum = 0;
  if (cs != checksum(packet))
    return;

  if (packet.seqnum != b.expectedseqnum) {
    simulation->tolayer3(B, b.last_ack);
    return;
  }

  struct msg message;
  memcpy(message.data, packet.payload, 20);
  simulation->tolayer5(B, message);

  memset(&b.last_ack, 0, sizeof(b.last_ack));
  b.last_ack.acknum = b.expectedseqnum;
  b.last_ack.checksum = checksum(b.last_ack);
  simulation->tolayer3(B, b.last_ack);
  b.expectedseqnum++;
}


// ***************************************************************************
// * Called when A's timer goes off
// ***************************************************************************
void A_timerinterrupt()
{
  struct pkt *p;

  std::cout << "Side A's timer has gone off." << std::endl;

  simulation->starttimer(A, TIMEOUT);

  for (p = a.sent_pkts->base; p < a.sent_pkts->base + min(a.sent_pkts->len, 20); ++p)
    simulation->tolayer3(A, *p);
}

// ***************************************************************************
// * Called when B's timer goes off
// ***************************************************************************
void B_timerinterrupt()
{
  std::cout << "Side B's timer has gone off." << std::endl;
}
