#include "simulator.h"


// a buffer-like structure to store unacknowledged packets
struct pkt_window {
  unsigned len, cap;
  struct pkt *pkts;
  struct pkt *base;
};


void pkt_window_init(pkt_window **pw, unsigned cap);
void pkt_window_new_pkt(pkt_window *pw, struct pkt packet, int base);
void pkt_window_update_base(pkt_window *pw, int base);
