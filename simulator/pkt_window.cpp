#include "pkt_window.h"

#include <string.h>
#include <stddef.h>
#include <stdlib.h>


void pkt_window_init(pkt_window **pw, unsigned cap)
{
  *pw = (struct pkt_window *) malloc(sizeof(struct pkt_window));
  (*pw)->len = 0;
  (*pw)->cap = cap;
  (*pw)->pkts = (struct pkt *) malloc(sizeof(struct pkt) * cap);
  (*pw)->base = (*pw)->pkts;
}


void pkt_window_new_pkt(pkt_window *pw, struct pkt packet, int base)
{
  struct pkt *p;
  ptrdiff_t d;

  if (pw->len + 1 > pw->cap) {
    if (pw->pkts == pw->base) {
      pw->cap *= 2;
      pw->pkts = (struct pkt *) realloc(pw->pkts, sizeof(struct pkt) * pw->cap);
      for (p = pw->pkts; p < pw->pkts + pw->len; ++p)
        if (p->seqnum == base) {
          pw->base = p;
          break;
        }
    } else {
      d = pw->base - pw->pkts;
      memmove(pw->pkts, pw->base, sizeof(struct pkt) * (pw->len - d));
      pw->len -= d;
      pw->base = pw->pkts;
    }
  }

  memcpy(pw->pkts + pw->len++, &packet, sizeof(struct pkt));
}


void pkt_window_update_base(pkt_window *pw, int base)
{
  struct pkt *p;

  for (p = pw->base; p < pw->pkts + pw->len; ++p)
    if (p->seqnum == base) {
      pw->base = p;
      break;
    }
}
